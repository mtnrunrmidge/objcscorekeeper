//
//  ViewController.h
//  ObjCScoreKeeper
//
//  Created by Margaret Schroeder on 9/21/17.
//  Copyright © 2017 Margaret Schroeder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *team1ScoreLbl;
@property (weak, nonatomic) IBOutlet UILabel *team2ScoreLbl;
@property (weak, nonatomic) IBOutlet UIStepper *stepper1;
@property (weak, nonatomic) IBOutlet UIButton *ResetBtn;
@property (weak, nonatomic) IBOutlet UIStepper *stepper2;
- (IBAction)Stepper1Clicked:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *team2text;
@end

