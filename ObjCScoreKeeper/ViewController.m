//
//  ViewController.m
//  ObjCScoreKeeper
//
//  Created by Margaret Schroeder on 9/21/17.
//  Copyright © 2017 Margaret Schroeder. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *team1text;

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Stepper1Clicked:(id)sender {
    self.team1ScoreLbl.text =  [NSString stringWithFormat: @"%.0f",_stepper1.value];
}
- (IBAction)Stepper2Clicked:(id)sender {
     self.team2ScoreLbl.text =  [NSString stringWithFormat: @"%.0f",_stepper2.value];
}
- (IBAction)ResetBtnClicked:(id)sender {
    _stepper1.value = 0;
    _stepper2.value = 0;
    self.team1ScoreLbl.text = [NSString stringWithFormat: @"%.0f", _stepper1.value];
    self.team2ScoreLbl.text = [NSString stringWithFormat: @"%.0f", _stepper2.value];
}

-(BOOL) keyboardDismiss: (UITextField*) textField {
  
    [self.team1text resignFirstResponder];
    return YES;
}
- (IBAction)dismissKeyboard:(id)sender {
    [self.team1text resignFirstResponder];
    [self.team2text resignFirstResponder];
}
@end
